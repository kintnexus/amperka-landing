FROM ubuntu:18.04

WORKDIR /landing

ADD . .

RUN apt update \
    && apt install python3 python3-pip -y \
    && pip3 install flask

EXPOSE 3000

CMD ["python3", "10-landing.py"]
